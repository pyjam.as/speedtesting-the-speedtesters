import random
import time

import requests


sources = ["speedtest.net", "fast", "sonic"]
pi_ids = ["1", "2", "3"]


while True:
    r = requests.post("http://localhost:5000/speedtest/", json={"name":
                          random.choice(sources), "pi_id":
                          random.choice(pi_ids), "upload_bits_per_second":
                          random.randint(10, 200), "download_bits_per_second":
                          random.randint(10, 200) })
    r.raise_for_status()
    time.sleep(2)
