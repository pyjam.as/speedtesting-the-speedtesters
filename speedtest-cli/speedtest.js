#!/usr/bin/env node

const puppeteer = require("puppeteer");
const Observable = require("zen-observable");
const delay = require("delay");
const async = require("async");

const sleep = (milliseconds) => {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
};

async function runtest(observer, endpoint, callback, start_instructions) {
  const browser = await puppeteer.launch({
    args: ["--no-sandbox"],
    headless: true,
  });

  const page = await browser.newPage();
  await page.goto(endpoint);

  await sleep(1000);
  if (start_instructions && start_instructions.length >= 1) {
    for (let i = 0; i < start_instructions.length; i++) {
      let instruction = start_instructions[i];
      await page.click(instruction);
      await sleep(1000);
    }
  }

  await init(browser, page, observer, callback);
}

async function init(browser, page, observer, callback) {
  let previousResult;
  /* eslint-disable no-constant-condition, no-await-in-loop */
  while (true) {
    const result = await page.evaluate(callback);

    if (result.isDone) {
      browser.close();
      observer.next(result);
      observer.complete();
      return;
    }

    previousResult = result;

    await delay(100);
  }
}

let sneller = new Observable((observer) => {
  const endpoint = "http://achtbaan.nikhef.nl/sneller/";
  let callback = () => {
    const $ = document.querySelector.bind(document);
    return {
      speedtestName: "sneller",
      downloadSpeed: Number($("#dlText").textContent),
      unit: $(".testArea .unit").textContent,
      isDone: Number($("#ulText").textContent) > 0,
    };
  };
  let start_instructions = ["#startStopBtn"];

  runtest(observer, endpoint, callback, start_instructions);
});

// TODO
let testMyNet = new Observable((observer) => {
  const endpoint = "https://testmy.net/";
  let callback = () => {
    const $ = document.querySelector.bind(document);
    return {
      speedtestName: "testMyNet",
      downloadSpeed: Number($(".color22")?.textContent),
      isDone: false,
    };
  };
  let start_instructions = [
    ".qc-cmp2-summary-buttons:nth-child(1)",
    "#testBtnMn",
  ];
  runtest(observer, endpoint, callback, start_instructions);
});

let fast = new Observable((observer) => {
  // Wrapped in async IIFE as `new Observable` can't handle async function
  const endpoint = "https://fast.com";
  let callback = () => {
    const $ = document.querySelector.bind(document);
    return {
      speedtestName: "fast",
      downloadSpeed: Number($("#speed-value").textContent),
      unit: $("#speed-units").textContent,
      isDone: Boolean(
        $("#speed-value.succeeded") && $("#upload-value.succeeded")
      ),
    };
  };

  runtest(observer, endpoint, callback);
});

let yousee = new Observable((observer) => {
  // Wrapped in async IIFE as `new Observable` can't handle async function
  const endpoint = "https://yousee.dk/hjaelp/hastighedstest";
  let callback = () => {
    const $ = document.querySelector.bind(document);
    const $A = document.querySelectorAll.bind(document);
    return {
      speedtestName: "yousee",
      downloadSpeed: Number($A("div.MuiBox-root > div > div > h1")[1].textContent),
      unit: "Mbps",
      isDone: Boolean($A("div.MuiBox-root > div > div > h1")[2].textContent !== "0"),
    };
  };
  let start_instructions = ["#declineButton", ".MuiButton-ctaPrimary"];
  runtest(observer, endpoint, callback, start_instructions);
});

let speedtestNet = new Observable((observer) => {
  // Wrapped in async IIFE as `new Observable` can't handle async function
  const endpoint = "https://speedtest.net";
  let callback = () => {
    const $ = document.querySelector.bind(document);
    return {
      speedtestName: "speedtestNet",
      downloadSpeed: Number($(".result-data-value.download-speed").textContent),
      unit: $(".gauge-unit-text").textContent,
      isDone: !Boolean($(".result-item-upload.inactive")),
    };
  };
  let start_instructions = ["#onetrust-accept-btn-handler", ".start-text"];
  runtest(observer, endpoint, callback, start_instructions);
});

let speedOfMe = new Observable((observer) => {
  const endpoint = "https://speedof.me/";
  let callback = () => {
    const $ = document.querySelector.bind(document);
    let unitRegex = /\(([^)]+)\)/;
    return {
      speedtestName: "speedOfMe",
      downloadSpeed: Number($(".progress-download-color").textContent),
      // matches the first set of chars between parentheses
      unit: unitRegex
        .exec($(".progress-desc-color").textContent)[0]
        .replace(/[()]/g, ""),
      isDone: Boolean($(".progress-upload-color").textContent !== "-"),
    };
  };
  let start_instructions = [".cc-compliance", "#start_test_btn"];
  runtest(observer, endpoint, callback, start_instructions);
});

const conversion = {
  gbps: 1_000_000_000,
  mbps: 1_000_000,
  kbps: 1_000,
};

// working: sneller, fast, speedOfMe
async function speedtest(speedtestProvider, runs) {
  let list = [];
  for (let i = 0; i < runs; i++) {
    list[i] = speedtestProvider;
  }
  download = 0
  await async.each(list, async (observable) => {
    // eval to accept strings as defined observables
    await eval(observable).forEach((item) => {
      let speed = item.downloadSpeed;
      let unit = item.unit.toLowerCase();
      bitsPerSecond = speed * conversion[unit];
	  download += bitsPerSecond
      //todo: replace by push to some endpoint
    });
  });
  console.log(`{"download": ${download}}`);
}

module.exports = {
  speedtest,
};
