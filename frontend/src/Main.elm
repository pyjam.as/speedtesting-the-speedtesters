port module Main exposing (..)

import Browser
import Chart as C
import Chart.Attributes as CA
import Dict exposing (Dict)
import Element as El
import Element.Background as Background
import Element.Font as Font
import Html
import Html.Attributes
import Json.Decode as JD
import Set
import Stats
import Svg as S
import Time


colorMap =
    Dict.fromList
        [ ( "fast", CA.red )
        , ( "sneller", CA.orange )
        , ( "speedOfMe", CA.yellow )
        , ( "speedtestNet", CA.blue )
        , ( "yousee", CA.green )
        ]


nameMap =
    Dict.fromList
        [ ( "fast", "fast.com" )
        , ( "sneller", "achtbaan.nikhef.nl" )
        , ( "speedOfMe", "speedof.me" )
        , ( "yousee", "yousee.dk" )
        , ( "speedtestNet", "speedtest.net" )
        ]


port messageReceiver : (String -> msg) -> Sub msg


main : Program () Model Msg
main =
    Browser.element
        { init =
            \_ ->
                ( { data = [], error = Nothing }
                , Cmd.none
                )
        , update = update
        , view = view
        , subscriptions = \_ -> messageReceiver WebSocketReceive
        }


type alias DataPoint =
    { name : String
    , piId : String
    , upload : Int
    , download : Int
    , timestamp : Int
    }


type alias Model =
    { data : List DataPoint
    , error : Maybe String
    }


type Msg
    = WebSocketReceive String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        WebSocketReceive data ->
            let
                dps : Result JD.Error (List DataPoint)
                dps =
                    JD.decodeString decodeDataPoints data
            in
            ( case dps of
                Ok newDps ->
                    { model | data = List.append model.data newDps }

                Err e ->
                    { model | error = Just <| JD.errorToString e }
            , Cmd.none
            )



-- JSON ENCODING / DECODING


decodeDataPoints : JD.Decoder (List DataPoint)
decodeDataPoints =
    JD.list <|
        JD.map5
            (\n u d t i -> { name = n, upload = u, download = d, timestamp = t, piId = i })
            (JD.field "name" JD.string)
            (JD.field "upload" JD.int)
            (JD.field "download" JD.int)
            (JD.field "timestamp" JD.int)
            (JD.field "pi_id" JD.string)



-- VISUAL STUFF


noFocusStyle : El.FocusStyle
noFocusStyle =
    { borderColor = Nothing, backgroundColor = Nothing, shadow = Nothing }


bgColor : El.Color
bgColor =
    El.rgb 0.57 0.27 0.26


fgColor : El.Color
fgColor =
    El.rgb 1.0 1.0 1.0


graphColor =
    El.rgb 1.0 1.0 1.0


logo : El.Element msg
logo =
    El.image
        [ El.height <| El.px 80, El.centerX, El.paddingXY 80 0 ]
        { src = "/logo.png"
        , description = "The pyjam.as mascot: a cartoonish green snake wearing a blue pyjam.as"
        }


view : Model -> Html.Html Msg
view model =
    El.layoutWith
        { options = [ El.focusStyle noFocusStyle ] }
        [ El.width El.fill
        , Background.color bgColor
        , Font.color fgColor
        ]
    <|
        El.column [ El.width El.fill, El.centerX, El.spacing 30 ]
            [ El.el [ El.height <| El.px 50 ] <| El.text ""
            , El.row [ El.width El.fill, El.centerX ]
                [ logo
                , El.el [ Font.size 80, Font.bold, Font.italic, El.centerX ] <| El.text "Speedtest Speedtest"
                ]
            , model.error
                |> Maybe.map El.text
                |> Maybe.withDefault (El.text "")
            , viewChart model.data
            , El.el [ El.height <| El.px 100 ] <| El.text ""
            , showBarGraph model.data
            ]


viewChart : List DataPoint -> El.Element msg
viewChart data =
    let
        width =
            1000

        height =
            300

        sourceNames =
            data
                |> List.map .name
                |> Set.fromList
                |> Set.toList

        makeSeries sourceName =
            C.series
                (.timestamp >> toFloat)
                [ C.interpolated (.download >> toFloat) [ CA.color (colorMap |> Dict.get sourceName |> Maybe.withDefault CA.gray) ] []
                , C.interpolated (.upload >> (*) -1 >> toFloat) [] []
                ]
                (data |> List.filter (\d -> d.name == sourceName))

        series =
            List.map makeSeries sourceNames
    in
    C.chart
        [ CA.width width
        , CA.height height
        ]
        ([ C.xAxis []
         , C.xLabels [ CA.color "white", CA.format timestampStr ]
         , C.yAxis []
         , C.yLabels
            [ CA.color "white"
            , CA.withGrid
            , CA.format (\num -> String.fromFloat (num / 1000) ++ " Mbps")
            ]
         ]
            ++ series
        )
        |> El.html
        |> El.el
            [ El.height <| El.px height
            , El.width <| El.px width
            , El.centerX
            ]



-- Bar charts pr. name


showBarGraph : List DataPoint -> El.Element msg
showBarGraph data =
    let
        width =
            150

        height =
            300

        agg =
            Stats.groupBy .name
                >> Stats.agg
                    (\{ key, download } ->
                        { label = key
                        , mean = Stats.mean (List.map toFloat download)
                        , max = List.maximum (List.map toFloat download) |> Maybe.withDefault 0.0
                        }
                    )

        sourceNames =
            Dict.keys colorMap

        mkChart sourceName =
            C.chart
                [ CA.height height
                , CA.width width
                , CA.domain
                    [ CA.lowest 0.0 CA.orLower
                    , CA.highest 1200000.0 CA.orHigher
                    ]
                ]
                [ C.grid []
                , C.yLabels [ CA.color "white", CA.withGrid, CA.format (\num -> String.fromFloat (num / 1000) ++ " Mbps") ]
                , C.labelAt CA.middle
                    .max
                    [ CA.moveUp 30, CA.fontSize 30, CA.color (colorMap |> Dict.get sourceName |> Maybe.withDefault CA.gray) ]
                    [ S.text (nameMap |> Dict.get sourceName |> Maybe.withDefault sourceName) ]
                , C.labelAt CA.middle
                    .min
                    [ CA.moveDown 18, CA.moveLeft 32, CA.color "white" ]
                    [ S.text "Avg" ]
                , C.labelAt CA.middle
                    .min
                    [ CA.moveDown 18, CA.moveRight 32, CA.color "white" ]
                    [ S.text "Max" ]
                , C.bars []
                    [ C.bar .mean
                        [ CA.borderWidth 2, CA.opacity 0.9, CA.color (colorMap |> Dict.get sourceName |> Maybe.withDefault CA.gray) ]
                        |> C.named "Average"
                    , C.bar
                        .max
                        [ CA.borderWidth 2, CA.opacity 0.4, CA.color (colorMap |> Dict.get sourceName |> Maybe.withDefault CA.gray) ]
                        |> C.named "Average"
                    ]
                    (agg data |> List.filter (\x -> x.label == sourceName) |> List.take 100)
                ]
    in
    sourceNames
        |> List.map mkChart
        |> List.map
            (List.singleton
                >> Html.div
                    [ Html.Attributes.style "display" "inline-block"
                    , Html.Attributes.style "width" <| String.fromInt width
                    , Html.Attributes.style "padding-left" "55px"
                    , Html.Attributes.style "padding-right" "55px"
                    , Html.Attributes.style "height" <| String.fromInt height
                    ]
            )
        |> Html.div [ Html.Attributes.style "text-align" "center", Html.Attributes.style "width" "100vw" ]
        |> El.html
        |> El.el
            [ El.width <| El.px width
            , El.height <| El.px height
            ]



-- TIME STUFF


timestampStr : Float -> String
timestampStr =
    round >> (*) 1000 >> Time.millisToPosix >> timeToString


timeToString : Time.Posix -> String
timeToString time =
    (Time.toMonth Time.utc time |> monthStr)
        ++ " "
        ++ String.fromInt (Time.toDay Time.utc time)
        ++ " "
        ++ String.fromInt (Time.toHour Time.utc time)
        ++ ":"
        ++ String.fromInt (Time.toMinute Time.utc time)


monthStr : Time.Month -> String
monthStr month =
    case month of
        Time.Jan ->
            "Jan"

        Time.Feb ->
            "Feb"

        Time.Mar ->
            "Mar"

        Time.Apr ->
            "Apr"

        Time.May ->
            "Maj"

        Time.Jun ->
            "Jun"

        Time.Jul ->
            "Jul"

        Time.Aug ->
            "Aug"

        Time.Sep ->
            "Sep"

        Time.Oct ->
            "Okt"

        Time.Nov ->
            "Nov"

        Time.Dec ->
            "Dec"
