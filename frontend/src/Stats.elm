module Stats exposing (agg, groupBy, last, mean, quantile)

import Dict exposing (Dict)


type alias Measurement =
    { name : String
    , piId : String
    , upload : Int
    , download : Int
    , timestamp : Int
    }


type alias GroupedMeasurement key =
    { key : key
    , upload : List Int
    , download : List Int
    , timestamp : List Int
    }


empty key =
    GroupedMeasurement key [] [] []


flatten : key -> List Measurement -> GroupedMeasurement key
flatten key =
    let
        combine m gm =
            GroupedMeasurement
                key
                (.upload m :: .upload gm)
                (.download m :: .download gm)
                (.timestamp m :: .timestamp gm)
    in
    List.foldr combine (empty key)



--


groupBy : (a -> comparable) -> List a -> Dict comparable (List a)
groupBy keyfn list =
    List.foldr
        (\x acc ->
            Dict.update (keyfn x) (Maybe.map ((::) x) >> Maybe.withDefault [ x ] >> Just) acc
        )
        Dict.empty
        list


agg : (GroupedMeasurement k -> v) -> Dict k (List Measurement) -> List v
agg aggregator =
    Dict.map
        (\k v ->
            v
                |> flatten k
                |> aggregator
        )
        >> Dict.values



-- Aggregators


mean : List Float -> Float
mean xs =
    let
        sum =
            List.foldl (+) 0 xs
    in
    sum / toFloat (List.length xs)


last : List a -> Maybe a
last list =
    case list of
        [] ->
            Nothing

        [ x ] ->
            Just x

        _ :: rest ->
            last rest


quantile : Float -> List Float -> Maybe Float
quantile p values =
    if p <= 0 then
        List.head values

    else if p >= 1 then
        last values

    else
        case values of
            [] ->
                Nothing

            [ head ] ->
                Just head

            x :: y :: _ ->
                let
                    n =
                        List.length values |> toFloat

                    i =
                        (n - 1) * p

                    i0 =
                        floor i

                    value0 =
                        getAt i0 values |> Maybe.withDefault x

                    value1 =
                        getAt (i0 + 1) values |> Maybe.withDefault y
                in
                Just <| value0 + (value1 - value0) * (i - toFloat i0)



-- Helpers


getAt : Int -> List a -> Maybe a
getAt i list =
    case list of
        [] ->
            Nothing

        head :: rest ->
            if i == 0 then
                Just head

            else
                getAt (i - 1) rest
