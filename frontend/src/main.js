import { Elm } from "./Main.elm";

const WS_URL = "wss://backend.speedtest.pyjam.as/results";

const node = document.getElementById("app");
const app = Elm.Main.init({ node });

const socket = new WebSocket(WS_URL);

// glue ws & elm together
socket.addEventListener("message", (event) => {
  app.ports.messageReceiver.send(event.data);
});
