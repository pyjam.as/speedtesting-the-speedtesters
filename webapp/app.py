from dataclasses import dataclass
from datetime import datetime
from typing import List, Optional
import asyncio
import json
import time

from databases import Database
from quart import Quart, websocket
from quart_schema import QuartSchema, validate_request
from sqlalchemy import Column, DateTime, Integer, String
from sqlalchemy.orm import declarative_base
from sqlalchemy.sql import func, insert, select


app = Quart(__name__)
QuartSchema(app)

Base = declarative_base()
CONNECTION_STRING = "postgresql+asyncpg://postgres:secretlol@db"
database = Database(CONNECTION_STRING)


@dataclass
class SpeedtestDatapointIn:
    name: str
    pi_id: str
    upload_bits_per_second: int
    download_bits_per_second: int


class Datapoint(Base):
    __tablename__ = "datapoint"

    id = Column(Integer, primary_key=True)
    pi_id = Column(String)
    name = Column(String)
    upload = Column(Integer)
    download = Column(Integer)
    timestamp = Column(DateTime(timezone=True), server_default=func.now())


@app.before_serving
async def create_db():
    await database.connect()
    async with database.transaction():
        await database.execute(query="create table if not exists datapoint (id serial primary key, pi_id text, name text, upload int, download int, timestamp timestamp default now());")


async def insert_datapoint(data: SpeedtestDatapointIn):
    async with database.transaction():
        await database.execute(query=insert(Datapoint), values={"name": data.name, "pi_id": data.pi_id, "upload": data.upload_bits_per_second, "download": data.download_bits_per_second})


@app.post("/speedtest/")
@validate_request(SpeedtestDatapointIn)
async def add_datapoint(data: SpeedtestDatapointIn):
    await insert_datapoint(data)
    return {}


async def get_results(since: Optional[datetime]):
    if since is None:
        query = select(Datapoint)
    else:
        query = select(Datapoint).where(Datapoint.timestamp > since)
    async with database.transaction():
        results = await database.fetch_all(query=query)
        results.sort(key=lambda r: r.timestamp)
        return results


def to_ws_dict(self):
    return {
        "name": self.name,
        "pi_id": self.pi_id,
        "upload": self.upload,
        "download": self.download,
        "timestamp": int(time.mktime(self.timestamp.timetuple()))
    }


@app.websocket("/results")
async def ws_results() -> None:
    since = None
    while True:
        results = await get_results(since=since)
        if results:
            await websocket.send(json.dumps(list(map(to_ws_dict, results))))
            since = results[-1].timestamp
        await asyncio.sleep(3)


if __name__ == "__main__":
    app.run("0.0.0.0")
