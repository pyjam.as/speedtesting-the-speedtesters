import json
import random
import subprocess
import sys

import requests


speedtests = [
    "fast",
    "speedOfMe",
    "sneller",
    "speedtestNet",
    "yousee",
]

PI_ID = sys.argv[1]


while True:
    random.shuffle(speedtests)
    for speedtest in speedtests:
        print(speedtest)
        s = subprocess.check_output(["./cli.js", speedtest, '2'])
        j = json.loads(s)
        print(j)
        # upload = j["upload"] / 1000
        download = j["download"] / 1000
        r = requests.post(
            "https://backend.speedtest.pyjam.as/speedtest/", json={
                "name": speedtest,
                "pi_id": PI_ID,
                "upload_bits_per_second": 1,
                "download_bits_per_second": download,
            }
        )
    print("done", speedtest, r.text)
